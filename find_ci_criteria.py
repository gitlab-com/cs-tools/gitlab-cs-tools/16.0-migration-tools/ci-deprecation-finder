#!/usr/bin/env python3

import gitlab
import json
import argparse
import yaml
import collections
import re
import csv

# creating a list of paths in the CI yml file, so that I check them for criteria
def make_path_list(json, path_list = set(), path = ""):
    if isinstance(json, dict):
        for key, value in json.items():
            new_path = f"{path}:{key}" if path else key
            make_path_list(value, path_list, new_path )
    elif isinstance(json, list):
        for index, value in enumerate(json):
            new_path = f"{path}:{index}" if path else str(index)
            make_path_list(value, path_list, new_path)
    else:
        path_list.add(path + ":" + str(json))
    return path_list

# getting the merged project CI yml
def get_merged_yml(gl, gl_project):
    try:
        lint_result = gl_project.ci_lint.get(include_merged_yaml=True)
        if lint_result.valid == True:
            return lint_result.merged_yaml
        else:
            print("YML invalid or missing")
            return None
    except Exception as err:
        print("[Warn] Could not lint CI file:")
        project_object = gl.projects.get(gl_project.id)
        if project_object.repository_access_level == "disabled" or project_object.empty_repo:
            print("[Warn] Project %s has no repo, skipping" % gl_project.id)
        else:
            print(err)
        return None

# get all projects for a group
def get_group_projects(gl, groups):
    projects = []
    for topgroup in groups:
        group = gl.groups.get(topgroup)
        group_projects = group.projects.list(iterator=True, include_subgroups=True, lazy=True)
        for group_project in group_projects:
            projects.append(gl.projects.get(group_project.id, lazy=True))
    return projects

# check if the defined deprecation criteria match our path list
def check_criteria(project, ci_yml_pathlist, criteria):
    criteria_findings = {}
    
    for crit_key in criteria:
        criterium = criteria[crit_key]
        contains_check = False
        lacks_check = True

        for ci_keyword_path in ci_yml_pathlist:

            if "contains" in criterium:
                # if any regex matches, set true
                for regex in criterium["contains"]:
                    if bool(re.search(regex, ci_keyword_path)):
                        contains_check = True            
            
            if "lacks" in criterium:
                # if any regex matches, set false ("doesn't lack")
                for regex in criterium["lacks"]:
                    if bool(re.search(regex, ci_keyword_path)):
                        lacks_check = False

        if contains_check and lacks_check:
            criteria_findings[crit_key] = True

    for crit_key in criteria:
        if crit_key not in criteria_findings:
            criteria_findings[crit_key] = False
    return criteria_findings


def sort_by_group(project_adoptions):
    # sort groups alphabetically, sort projects in groups alphabetically, produce a list
    sorted_projects = {}
    for project in project_adoptions:
        project_group = project["path"]
        project_group = project_group[0:project_group.rfind("/")]
        project["group"] = project_group
        if project_group in sorted_projects:
            sorted_projects[project_group].append(project)
            sorted_projects[project_group] = sorted(sorted_projects[project_group], key=lambda s: s["name"])
        else:
            sorted_projects[project_group] = [project]
    sorted_projects = collections.OrderedDict(sorted(sorted_projects.items()))
    project_list = []
    for group in sorted_projects:
        projects = sorted_projects[group]
        for project in projects:
            project_list.append(project)
    return project_list

def get_group_count(project_adoptions):
    groups = set()
    for project in project_adoptions:
        groups.add(project["group"])
    return len(groups)

def get_criteria_report(gl, projects, criteria, args):
    project_adoptions = []
    pathlists = []
    for project in projects:
        print("[Info] Checking criteria in project %s" % (project.id))
        has_ci = False
        parsed_ci = {}
        merged_yml = get_merged_yml(gl, project)
        if merged_yml:
            parsed_ci = yaml.safe_load(merged_yml)
        else:
            has_ci = False
        pathlist = set()
        if parsed_ci:
            try:
                pathlist = make_path_list(parsed_ci, pathlist)
                has_ci = True
            except Exception as e:
                print("[ERROR] Could not parse keys for project %s" % (project.id))
                has_ci = False
                print(e)

        project = gl.projects.get(project.id)
        project_adoption = {}
        project_adoption["name"] = project.name
        project_adoption["path"] = project.attributes["path_with_namespace"]
        project_adoption["url"] = project.attributes["web_url"]
        project_adoption["has_ci"] = has_ci
        project_adoption.update(check_criteria(project, pathlist, criteria))
        project_adoptions.append(project_adoption)
        project_paths = {}
        project_paths["name"] = project.name
        project_paths["path"] = project.attributes["path_with_namespace"]
        project_paths["url"] = project.attributes["web_url"]
        project_paths["paths"] = sorted(list(pathlist))
        pathlists.append(project_paths)
    return project_adoptions, pathlists

parser = argparse.ArgumentParser(description='Create report for GitLab issues')
parser.add_argument('token', help='API token able to read the requested projects')
parser.add_argument('configfile', help='YML file that defines requested groups, projects and adoption parameters')
parser.add_argument('--gitlab', help='URL of the gitlab instance', default="https://gitlab.com/")

args = parser.parse_args()

gl = gitlab.Gitlab(args.gitlab, private_token=args.token, retry_transient_errors=True)

configfile = args.configfile
groups = []
projects = []
criteria = {}
frameworks = {}
compliance_project = None

with open(configfile, "r") as c:
    config = yaml.load(c, Loader=yaml.FullLoader)
    if "groups" in config:
        groups = config["groups"]
        projects = get_group_projects(gl, groups)
    else:
        if not "projects" in config:
            print("Retrieving all projects the token has access to")
            projects = gl.projects.list(iterator=True, lazy=True)
        else:
            for project in config["projects"]:
                projects.append(gl.projects.get(project, lazy=True))

    if "criteria" in config:
        criteria = config["criteria"]
    else:
        print("No criteria provided, exiting")

project_criteria = get_criteria_report(gl, projects, criteria, args)
pathlists = project_criteria[1]
project_criteria = sort_by_group(project_criteria[0])

with open("project_criteria.json", "w") as reportfile_json:
    json.dump(project_criteria, reportfile_json, indent = 2)

with open("ci_deprecation_report.csv","w") as reportfile_csv:
    header_fields = ['path', 'url', 'has_ci']
    first_row = header_fields.copy()
    for crit_key in criteria:
        first_row.append('=HYPERLINK("%s","%s")' % (criteria[crit_key]["doc"], criteria[crit_key]["name"]))
     
    writer = csv.writer(reportfile_csv)
    writer.writerow(first_row)

    for project in project_criteria:
        row = []
        for field in header_fields:
            row.append(project[field])
        for crit_key in criteria:
            row.append(project[crit_key])
        writer.writerow(row)


with open("criteria.json", "w") as adoptionfile:
    json.dump(criteria, adoptionfile, indent = 2)

with open("ci_paths.json", "w") as pathfile:
    json.dump(pathlists, pathfile, indent = 2)
